import 'package:flutter/material.dart';
import 'dart:async';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
 //for the TextField
  String _value = "";
  //for the checkbox
  bool checkvalue = false;
  //for the radio
  int group = 1;
  //for the switch
  bool state = false;
  //for the slider
  double sliderAmount = 0;
  void onchanged(String value) {
    setState(() => _value = "changed: $value");
  }

  void onsubmit(String value) {
    setState(() => _value = "onSubmit: $value");
  }

  //creating the object for the datetime:
  DateTime currentDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate)
      setState(() {
        currentDate = pickedDate;
      });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Input Widgets",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Input Widgets"),
        ),
        body: SingleChildScrollView(
                  child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(_value),
              TextField(
                decoration: InputDecoration(
                  labelText: "Hello",
                  hintText: "Hint",
                  icon: Icon(Icons.people),
                ),
                autocorrect: true,
                autofocus: true,
                keyboardType: TextInputType.text,
                onChanged: onchanged,
                onSubmitted: onsubmit,
              ),
              Row(
                children: [
                  Checkbox(
                      value: checkvalue,
                      onChanged: (bool value) {
                        setState(() {
                          checkvalue = value;
                        });
                      }),
                  Text("Flutter Developer"),
                ],
              ),
              Text("Lion is a ........"),
              Row(
                children: [
                  //making the radio buttons
                  Radio(
                    value: 1,
                    groupValue: group,
                    onChanged: (G) {
                      setState(() {
                        group = G;
                      });
                    },
                  ),
                  Text("Herbivoruous"),
                  Radio(
                    value: 2,
                    groupValue: group,
                    onChanged: (G) {
                      setState(() {
                        group = G;
                      });
                    },
                  ),
                  Text("Carnivorous"),
                ],
              ),
              Row(
                children: [
                  //making the switch
                  Switch(
                    value: state,
                    onChanged: (bool s) {
                      setState(() {
                        state = s;
                      });
                    },
                  ),
                  Text("Slide the bar for accepting lisence"),
                ],
              ),
              Row(
                children: [
                  //making the slider
                  Slider(
                    value: sliderAmount,
                    onChanged: (double delta) {
                      setState(() {
                        sliderAmount = delta;
                      });
                    },
                  ),
                ],
              ),
              Row(children: [
               RaisedButton(
              onPressed: () => _selectDate(context),
              child: Text('Select date'),
            ),
              ],),
            ],
          ),
        ),
      ),
    );
  }
}
